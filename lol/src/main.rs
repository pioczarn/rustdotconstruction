// Copyright (c) 2016 The Rouille developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.
#![allow(unused_imports)]
#![allow(unused_variables)]

extern crate mysql;
#[macro_use]
extern crate rouille;
extern crate serde;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate horrorshow;

extern crate rust_tags;

//use rust_tags::core::*;
use rust_tags::tags::*;
use rust_tags::tags::title;
use rust_tags::attributes::*;

use std::sync::Mutex;
use horrorshow::prelude::*;
use horrorshow::helper::doctype;

use mysql::Transaction;

use rouille::Request;
use rouille::Response;

#[derive(Serialize, Deserialize)]
struct NoteWithId {
    id: i32,
    title: String,
    body: String,
    #[serde(default = "default_completed")]
    completed: bool
}

#[derive(Serialize, Deserialize)]
struct Note {
    title: String,
    body: String,
    #[serde(default = "default_completed")]
    completed: bool
}

#[derive(Serialize, Deserialize)]
struct Status {
    ok: bool
}

#[derive(Serialize, Deserialize)]
struct CreatedNote {
    id: i32
}

fn default_completed() -> bool {
    return false;
}

fn main() {
    // This example demonstrates how to connect to a database and perform queries when the client
    // performs a request.
    // The server created in this example uses a REST API.

    // The first thing we do is try to connect to the database.
    //
    // One important thing to note here is that we wrap a `Mutex` around the connection. Since the
    // request handler can be called multiple times in parallel, everything that we use in it must
    // be thread-safe. By default the PostgresSQL connection isn't thread-safe, so we need a mutex
    // to make it thread-safe.
    //
    // Not wrapping a mutex around the database would lead to a compilation error when we attempt
    // to use the variable `db` from within the closure passed to `start_server`.

    let db = {
        let address = env!("MYSQL_PATH");
        let db = mysql::Pool::new(address);
        Mutex::new(db.expect("Failed to connect to database"))
    };

    // We perform some initialization for the sake of the example.
    // In a real application you probably want to have a migrations system. This is out of scope
    // of rouille.

    let init_sql = r"
        CREATE TABLE IF NOT EXISTS notesconstruction.notes (
            id SERIAL PRIMARY KEY,
            title TEXT NOT NULL,
            body TEXT NOT NULL,
            completed BOOL NOT NULL,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        );
    ";

    db.lock()
      .unwrap()
      .prep_exec(init_sql, ())
      .expect("Failed to initialize database");

    // Small message so that people don't need to read the source code.
    // Note that like all the other examples, we only listen on `localhost`, so you can't access this server
    // from any machine other than your own.
    println!("Now listening on localhost:8000");

    // Now the server starts listening. The `move` keyword will ensure that we move the `db` variable
    // into the closure. Not putting `move` here would result in a compilation error.
    //
    // Note that in an ideal world, `move` wouldn't be necessary here. Unfortunately Rust isn't
    // smart enough yet to understand that the database can't be destroyed while we still use it.
    rouille::start_server("0.0.0.0:8000", move |request| {
        // Since we wrapped the database connection around a `Mutex`, we lock it here before usage.
        //
        // This will give us exclusive access to the database connection for the handling of this
        // request. Unfortunately the consequence is that if a request is made while another one
        // is already being processed, the second one will have to wait for the first one to
        // complete.
        //
        // In a real application you probably want to create multiple connections instead of just
        // one, and make each request use a different connection.
        //
        // In addition to this, if a panic happens while the `Mutex` is locked then the database
        // connection will likely be in a corrupted state and the next time the mutex is locked
        // it will panic. This is another good reason to use multiple connections.

        let db = db.lock().unwrap();

        // Start a transaction so that if a panic happens during the processing of the request,
        // any change made to the database will be rolled back.
        let mut transaction = db.start_transaction(true, None, None).unwrap();

        transaction.query("USE notesconstruction").unwrap();

        // For better readability, we handle the request in a separate function.
        let response = note_routes(&request, &mut transaction);

        // If the response is a success, we commit the transaction before returning. It's only at
        // this point that data are actually written in the database.
        if response.is_success() {
            transaction.commit().unwrap();
        }

        response
    });
}

// This function actually handles the request.
fn note_routes(request: &Request, db: &mut Transaction) -> Response {
    router!(request,
            // (GET) (/) => {
            // For the sake of the example we just put a dummy route for `/` so that you see
            // something if you connect to the server with a browser.
            //    Response::text("Welcome to your first app :)")
            //},


            (GET) (/t) => {

        	    let page_title = "Notes App";
        	    let mytext = "Note App I am a variable mytext";
        	    let batman = "Gotham";
        	    let superman = "Superman will arrive shortly";

                //Response::text("Welcome to your first app :)")
                let actual = format!("{}", html! {
                    : doctype::HTML;
                    html {
                        head {
                            title : page_title;
                            link(rel="stylesheet", type="text/css", href="https://cloud.typography.com/7964312/7143592/css/fonts.css");
                        }
                        body {
                            // attributes

                            h1(id="heading") {
                	            div (style= "font-family: Giant Background A, Giant Background B;font-style: normal;font-weight: 400;font-size:30pt;")

                                // Insert escaped text
                                    : "Welcome to the NOTES.CONSTRUCTION App";
                            }

                            //style (font-family="Tugsten A;")
                            div (style= "font-family: Gotham A, Gotham B;font-style: normal;font-weight: 400;"){
                                // Insert raw text = (unescaped)
                                : batman;
                                br;
                                : "Batman is here";
                                br;
                                : superman;

                                form (action="/note", method="POST", enctype="text/plain");
                                input(type="text" ,name="Title", placeholder="Title");
                                br;

                                textarea (name="abcd", placeholder="your note here", cols="40", rows= "5"){}

                                //:Raw("</textarea>");
                                br;
                                input (type="submit");

                                //p (input type="submit")
                                //: Raw("<i>test raw/i> !")
                            }

                            div (style= "font-family: Tungsten A, Tungsten B;font-style: normal;font-weight: 400;font-size:30pt;") {
                                // Insert raw text = (unescaped)
                                : superman;
                            }

                            // a link
                            a  (href="/notes") {
                                : "A list of Notes";
                            }

                            h2 (id="test") {
                                // Insert raw text (unescaped)
                                : mytext;
                            }

                            ol(id="count") {
                                // You can embed for loops, while loops, and if statements.
                                @ for i in 0..10 {
                                    li(first? = (i == 0)) {
                                        // Format some text.
                                        : format_args!("{}", i+1)
                                    }
                                }
                            }
                            // You need semi-colons for tags without children.
                            br; br;

                            p {
                                // You can also embed closures.
                                |tmpl| {
                                    tmpl << "Easy!";
                                }
                            }
                        }
                    }
                });

                rouille::Response::html (actual)
            },


            (GET) (/) => {
                let page_title = "Welcome to the NOTES.CONSTRUCTION App";
        	    let batman = "Fill the form to save your note";
        	    let superman = "by gurugeek | coded entirely in Rust -18/2/2018";
                let vue_component = include_str!("app.js");

                //Response::text("Welcome to your first app :)")
                let actual = format!("{}", html! {
                    : doctype::HTML;
                    html {
                        head {
                            title : page_title;
                            link(rel="stylesheet", href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css", integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb", crossorigin="anonymous");
                            link(rel="stylesheet", type="text/css", href="https://cloud.typography.com/7964312/7143592/css/fonts.css");
                            script(type="text/javascript", src="https://unpkg.com/vue@2.3.4"): "";
                            script(type="text/javascript", src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.4"): "";
                        }
                        body {
                            h1(id="heading") {
                	            div (style= "font-family: Giant Background A, Giant Background B;font-style: normal;font-weight: 400;font-size:30pt;")
                                    : page_title;
                            }

                            div (style= "font-family: Gotham A, Gotham B;font-style: normal;font-weight: 400;") {
                                batman;
                                br;
                            }

                            div(id="root");

                            div (style= "font-family: Tungsten A, Tungsten B;font-style: normal;font-weight: 400;font-size:30pt;") {
                                : superman;
                            }

                            br;
                            script(type="text/javascript"): Raw(vue_component);
                        }
                    }
                });

                rouille::Response::html(actual)

            },

            //POST) (/submit) => {
            // This is the route that is called when the user submits the form of the
            // home page.

            // We query the data with the `post_input!` macro. Each field of the macro
            // corresponds to an element of the form.
            // If the macro returns an error (for example if a field is missing, which
            // can happen if you screw up the form or if the user made a manual request)
            // we return a 400 response.
            //  let data = try_or_400!(post_input!(request, {
            //     txt: String,
            //    files: Vec<rouille::input::post::BufferedFile>,
            //}));

            // We just print what was received on stdout. Of course in a real application
            // you probably want to process the data, eg. store it in a database.
            //  println!("Received data: {:?}", data);

            //  rouille::Response::html("Success! <a href=\"/\">Go back</a>.")
            //  },
            // }

            (GET) (/tags) => { //using rust tags :)

 	            let superman = "superman";
 	            let notes = "Notes.Construction";
 	            let joker = "why so serious";


 	            let frag = html(&[
                    head(&[title(&["My Blog".into()])]),
                    link(&[rel("stylesheet"), _type("text/css"), href("https://cloud.typography.com/7964312/7143592/css/fonts.css")]),

                    body(&[
                        div(&[
                            //"Jason Longshore".into(),
                            notes.into(),

                            br(),
                            br(),
                            rust_tags::attributes::style("font-family:Tungsten A, Tungsten B;font-style: normal;font-weight: 400;font-size:30pt;"),

                            rust_tags::tags::form(&[action("/note"), method("POST"), enctype("text/plain"),
                                                    input(&[_type("text"), name("title"), placeholder("Some text")]),
                                                    input(&[_type("submit")]),
                                                    textarea(&[name("notes"), cols("40"), rows("40")])
                            ]),
                            superman.into(),
                            br(),

                            //input(type="text" ,name="Title", placeholder="Some text");

                            rust_tags::tags::form(&[id("my-form"), "this is my <form>".into()]),
                            //rust_tags::tags::textarea(&[name("notes"), cols ("40"), rows ("5").into()]),

                            hr(&[]),

                            div(&[ //div to change font to gotham

                                rust_tags::attributes::style("font-family:Gotham A, Gotham B;font-style: normal;font-weight: 400;font-size:30pt;"),

                                joker.into(),
                                br(),

                                a(&[href("#"), "My Blog <hello world />".into()]),

                                br(),
                                br()
                            ]) //closed div

                        ])
                    ])
                ]);

                // When viewing the home page, we return an HTML document described below.
                rouille::Response::html(frag.data)
            },


        (GET) (/notes) => {
            let mut notes: Vec<NoteWithId> = vec![];

            for row in db.query("SELECT id, title, body, completed FROM notes ORDER BY id DESC").unwrap() {
                let (id, title, body, completed) = mysql::from_row(row.unwrap());
                let note = NoteWithId {
                    id: id,
                    title: title,
                    body: body,
                    completed: completed
                };
                notes.push(note);
            }

            Response::json(&notes)
        },

        (GET) (/note/{id: i32}) => {
            // This route returns the title of a note, if it exists.
            // Note that this code is a bit unergonomic, but this is mostly a problem with the
            // database client library and not rouille itself.
            // To do so, we first create a variable that will receive the title of the note.
            let mut title: Option<String> = None;
            // And then perform the query and write to `title`. This line can only panic if the
            // SQL is malformed.
            for row in db.prep_exec("SELECT title FROM notes WHERE id = ?", (id,)).unwrap() {
                title = Some(mysql::from_row(row.unwrap()));
            }

            // If `title` is still empty at this point, this means that the note doesn't
            // exist in the database. Otherwise, we return the title.

           let page_title = "wow";

            let actual = html! {
                : doctype::HTML;
                html {
                    head {
                        title : page_title;
                        link(rel="stylesheet", type="text/css", href="https://cloud.typography.com/7964312/7143592/css/fonts.css");
                    }
                    body {
                        // attributes
                        h1(style= "font-family: Tungsten A, Tungsten B;font-style: normal;font-weight: 400;font-size:30pt;") {
                            // Insert escaped text
                            : "This is your note"
                        }

                        //style (font-family="Tugsten A;")
                        p (style= "font-family: Gotham A, Gotham B;font-style: normal;font-weight: 400;"){
                            // Insert raw text = (unescaped)
                            : &title;
                            //: Raw("<i>test raw/i> !")
                        } } }
            }.into_string().unwrap();

            match title {
                Some(_title) => Response::html(actual),
                None => Response::empty_404(),
            }
        },


            (GET) (/notetag/{id: i32}) => {
                // This route returns the title of a note, if it exists.
                // Note that this code is a bit unergonomic, but this is mostly a problem with the
                // database client library and not rouille itself.
                // To do so, we first create a variable that will receive the title of the note.
                let mut note_title: Option<String> = None;
                // And then perform the query and write to `note_title`. This line can only panic if the
                // SQL is malformed.
                for row in db.prep_exec("SELECT title FROM notes WHERE id = ?", (id,)).unwrap() {
                    note_title = mysql::from_row(row.unwrap());
                }

                // let superman = "superman";


 	            let notes = "Notes.Construction";
 	            let joker = "why so serious";
                //let notetag = note_title;

                let mut note_title_cloned = note_title.clone();

 	            let frag = html(&[
                    head(&[title(&["My Blog".into()])]),
                    link(&[rel("stylesheet"), _type("text/css"), href("https://cloud.typography.com/7964312/7143592/css/fonts.css")]),

                    body(&[
                        div(&[
                            //"Jason Longshore".into(),
                            notes.into(),


                            br(),br(),
                            rust_tags::attributes::style("font-family:Tungsten A, Tungsten B;font-style: normal;font-weight: 400;font-size:30pt;"),

                            rust_tags::tags::form(&[action("/note"), method("POST"), enctype("text/plain"),


                                                    input(&[_type("text"), name("note_title"), placeholder("Some text")]),
                                                    input(&[_type("submit")]),
                                                    textarea(&[name("notes"), cols("40"), rows("40")])
                            ]),

                            br(),

                            //input(type="text" ,name="note_title", placeholder="Some text");
                            //rust_tags::tags::textarea(&[name("notes"), cols ("40"), rows ("5").into()]),
                            hr(&[]),

                            div(&[ //div to change font to gotham

                                rust_tags::attributes::style("font-family:Gotham A, Gotham B;font-style: normal;font-weight: 400;font-size:30pt;"),

                                joker.into(),
                                (note_title_cloned.unwrap()).into(),

                                //String::from_utf8(note_title.into().unwrap()),
                                //note_title,
                                br(),

                                a(&[href("#"), "My Blog <hello world />".into()]),

                                br(),
                                br()
                            ]) //closed div
                        ])
                    ])
                ]);

                match note_title {
                    Some(_note_title) => Response::html(frag.data),
                    None => Response::empty_404(),
                }
            },



            (PUT) (/note/{id: i32}) => {
                // This route modifies the title of an existing note.

                // We start by reading the body of the HTTP request into a `Note` from JSON.
                let payload: Note = try_or_400!(rouille::input::json_input(&request));

                // And write the title with a query. This line can only panic if the
                // SQL is malformed.
                let updated = db.prep_exec("UPDATE notes SET title = ?, body = ?, completed = ? WHERE id = ?",
                                         (&payload.title, &payload.body, &payload.completed, id)).unwrap();

                // We determine whether the note existed based on the number of rows that
                // were modified.
                if updated.affected_rows() >= 1 {
                    let status = Status { ok: true };
                    Response::json(&status)
                } else {
                    Response::empty_404()
                }
            },

            (POST) (/note) => {
                // This route creates a new note whose initial title is the body.

                // We start by reading the body of the HTTP request into a `Note` from JSON.
                let payload: Note = try_or_400!(rouille::input::json_input(&request));

                // To do so, we first create a variable that will receive the title.
                let mut id: Option<i32> = None;
                // And then perform the query and write to `title`. This line can only panic if the
                // SQL is malformed.

                let query = "INSERT INTO notes(title, body, completed) VALUES (?, ?, ?)";
                db.prep_exec(query, (&payload.title, &payload.body, &payload.completed)).unwrap();
                for row in db.query("SELECT LAST_INSERT_ID()").unwrap() {
                    id = Some(mysql::from_row(row.unwrap()));
                }

                if let Some(id_value) = id {
                    let created = CreatedNote {
                        id: id_value
                    };
                    let mut response = Response::json(&created);
                    response.status_code = 201;
                    response
                } else {
                    Response::empty_404()
                }
            },

            (DELETE) (/note/{id: i32}) => {
                // This route deletes a note. This line can only panic if the
                // SQL is malformed.
                db.prep_exec("DELETE FROM notes WHERE id = ?", (id,)).unwrap();
                let status = Status { ok: true };
                Response::json(&status)
            },

            // If none of the other blocks matches the request, return a 404 response.
            _ => Response::empty_404()
    )
}
