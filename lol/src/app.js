var Vue = new Vue({
  el: '#root',
  template: `
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
          <br><br>
          <div class="card">
            <div class="card-body">
                <form @submit.prevent>
                  <div class="input-group">
                    <input type="text" v-model="note.title" @keyup="checkForEnter($event)" class="form-control custom-input" :class="{ 'error': showError }" placeholder="Add your note">
                    <span class="input-group-btn">
                      <button class="btn custom-button" :class="{'btn-success' : !enableEdit, 'btn-warning' : enableEdit}" type="button"  @click="addNote">Save</button>
                    </span>
                  </div>
                  <div class="input-group">
                    <textarea rows="3" v-model="note.body" @keyup="checkForEnter($event)" class="form-control custom-input"></textarea>
                  </div>
                </form>
                <ul class="list-group">
                  <li class="list-group-item" :class="{ 'checked': note.completed, 'not-checked': !note.completed }" v-for="(note, noteIndex) in notes" @click="toggleNote(note, noteIndex)">
                    <div>
                      <span v-if="note.completed">[Done]</span>
                      <span :class="{ 'del': note.completed }">@{ note.title }</span>
                      <div class="btn-group float-right" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success btn-sm custom-button" @click.prevent.stop @click="editNote(note, noteIndex)">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm custom-button" @click.prevent.stop @click="deleteNote(note, noteIndex)">Delete</button>
                      </div>
                    </div>
                    <div v-if="note.body !== ''">
                      <span>@{ note.body }</span>
                    </div>
                  </li>
                </ul>
            </div>
          </div>
      </div>
    </div>
  </div>
  `,
  delimiters: ['@{', '}'],
  data: {
    showError: false,
    enableEdit: false,
    note: {id: '', title: '', body: '', completed: false},
    notes: []
  },
  mounted () {
    this.$http.get('notes').then(response => {
      this.notes = response.body;
    });
  },
  methods: {
    addNote(){
      if (this.note.title == '') {
        this.showError = true;
      } else {
        this.showError = false;
        if (this.enableEdit) {
          this.$http.put('note/'+this.note.id, this.note).then(response => {
            if (response.status == 200) {
              this.notes[this.note.noteIndex] = this.note;
            }
          });
          this.note = {id: '', title: '', completed: false};
          this.enableEdit = false;
        } else {
          const newNote = { title: this.note.title, body: this.note.body };
          this.$http.post('note', newNote).then(response => {
            if (response.status == 201) {
              this.notes = [
                {id: response.body.id, title: this.note.title, body: this.note.body, completed: false},
                ...this.notes
              ];
              this.note = {id: '', title: '', body: '', completed: false};
            }
          });
        }
      }
    },
    checkForEnter(event){
      if (event.key == "Enter") {
        this.addNote();
      }
    },
    toggleNote(note, noteIndex){
      var completedToggled = !note.completed;
      var params = {
        id: note.id,
        title: note.title,
        body: note.body,
        completed: completedToggled
      };

      this.$http.put('note/'+note.id, params).then(response => {
        if (response.status == 200) {
          this.notes[noteIndex].completed = completedToggled;
        }
      });
    },
    editNote(note, noteIndex){
      this.enableEdit = true;
      this.note = note;
      this.note.noteIndex = noteIndex;
    },
    deleteNote(note, noteIndex){
      if (confirm("Are you sure?")){
        this.$http.delete('note/'+note.id).then(response => {
          if (response.status == 200){
            this.notes.splice(noteIndex, 1);
            this.note = {id: '', title: '', body: '', completed: false};
          }
        });
      }
    }
  }
});
